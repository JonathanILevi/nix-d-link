{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let
	inherit (nixpkgs) pkgs;
	f = { mkDerivation, base, stdenv, hpack, dmd, cabal-install }:
			mkDerivation {
				pname = "Nix-D-Sample";
				version = "0.1.0";
				src = ./.;
				isLibrary = false;
				isExecutable = true;
				libraryHaskellDepends = [ base ];
				librarySystemDepends = [ dmd ];
				libraryToolDepends = [ hpack cabal-install ];
				executableHaskellDepends = [ base ];
				doHaddock = false;
				prePatch = "hpack";
				license = "unknown";
				hydraPlatforms = stdenv.lib.platforms.none;
			};
	
	haskellPackages = if compiler == "default"
		then pkgs.haskellPackages
		else pkgs.haskell.packages.${compiler};
	
	variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;
	
	drv = variant (haskellPackages.callPackage f {});

in
	if pkgs.lib.inNixShell then drv.env else drv
